from flask_sqlalchemy import SQLAlchemy
from app import app

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class user(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50), nullable = False)
    email = db.Column(db.String(30), nullable = False)
    username = db.Column(db.String(30), nullable = False)
    password = db.Column(db.String(70), nullable = False)
    address = db.Column(db.String(50), nullable = False)
    is_admin = db.Column(db.Boolean, nullable = False, default = False)
    is_superadmin = db.Column(db.Boolean, nullable = False, default = False)

class order(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    type = db.Column(db.String(15), nullable=False)
    measure_length = db.Column(db.String(30), nullable=False)
    measure_width = db.Column(db.String(30), nullable=False)
    measure_type = db.Column(db.String(30), nullable=False)
    color = db.Column(db.String(15), nullable = False)
    file = db.Column(db.String(100), nullable = True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

class measure(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    type = db.Column(db.String(15), nullable=False)
    unit = db.Column(db.String(15), nullable=False)
    unit_cost= db.Column(db.Float, nullable=False)

class catagory(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(30), nullable=False)
    image = db.Column(db.String(50), nullable=False)

class subcatagory(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(30), nullable=False)
    image = db.Column(db.String(50), nullable=False)
    type = db.Column(db.String(15), nullable=False)
    color = db.Column(db.String(10), nullable=False)
    size = db.Column(db.String(15), nullable=True)
    print = db.Column(db.String(20), nullable=True)
    corner_cut = db.Column(db.String(10), nullable=True)
    pages = db.Column(db.Integer, nullable=True)
    cost = db.Column(db.Float, nullable=False)
    c_id = db.Column(db.Integer, db.ForeignKey('catagory.id'))

# db.create_all()
